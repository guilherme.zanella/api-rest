package com.produtos.apirest.resources;

import com.produtos.apirest.models.Endereco;
import com.produtos.apirest.repository.EnderecoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api")
@CrossOrigin(origins = "*")
public class EnderecoResource {
    @Autowired
    EnderecoRepository enderecoRepository;

    @GetMapping("/enderecos")
    public List<Endereco> listaEnderecos() {
        return enderecoRepository.findAll();
    }

    @GetMapping("/endereco/{id}")
    public Endereco buscaEndereco(@PathVariable(value = "id") long id) {
        return enderecoRepository.findById(id);
    }

    @PostMapping("/endereco")
    public Endereco salvaEndereco(@RequestBody @Validated Endereco endereco) {
        return enderecoRepository.save(endereco);
    }

    @DeleteMapping("/endereco/{id}")
    public void deletaEndereco(@PathVariable(value = "id") long id) {
        if (enderecoRepository.findById(id) != null) {
            enderecoRepository.deleteById(id);
        }
    }

    @PutMapping("/endereco")
    public Endereco alteraEndereco(@RequestBody @Validated Endereco endereco) {
        if (enderecoRepository.findById(endereco.getId()) != null) {
            return enderecoRepository.save(endereco);
        } else {
            return null;
        }
    }
}
