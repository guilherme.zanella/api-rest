package com.produtos.apirest.resources;

import com.produtos.apirest.models.Cliente;
import com.produtos.apirest.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api")
@CrossOrigin(origins = "*")
public class ClienteResource {
    @Autowired
    ClienteRepository clienteRepository;

    @GetMapping("/clientes")
    public List<Cliente> listaClientes() {
        return clienteRepository.findAll();
    }

    @GetMapping("/cliente/{id}")
    public Cliente buscaCliente(@PathVariable(value = "id") long id){
        return clienteRepository.findById(id);
    }

    @PostMapping("/cliente")
    public Cliente salvaCliente(@RequestBody @Validated Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    @DeleteMapping("/cliente/{id}")
    public void deletaCliente(@PathVariable(value = "id") long id) {
        if (clienteRepository.findById(id) != null) {
            clienteRepository.deleteById(id);
        }
    }

    @PutMapping("/cliente")
    public Cliente alteraCliente(@RequestBody @Validated Cliente cliente) {
        if (clienteRepository.findById(cliente.getId()) != null) {
            return clienteRepository.save(cliente);
        } else {
            return null;
        }
    }
}
