package com.produtos.apirest.resources;

import com.produtos.apirest.models.Produto;
import com.produtos.apirest.repository.ProdutoRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/api")
@Api(value="API REST Produtos")
@CrossOrigin(origins="*")
public class ProdutoResource {
    @Autowired
    ProdutoRepository produtoRepository;

    @GetMapping("/produtos")
    @ApiOperation(value="Retorna lista de produtos")
    public List<Produto> listaProdutos() {
        return produtoRepository.findAll();
    }

    @GetMapping("/produto/{id}")
    @ApiOperation(value="Retorna um produto buscado por id")
    public Produto produtoUnico(@PathVariable(value="id") long id) {
        return produtoRepository.findById(id);
    }

    @PostMapping("/produto")
    @ApiOperation(value="Salva um produto")
    public Produto salvaProduto(@RequestBody @Validated Produto p) {
        return produtoRepository.save(p);
    }

    @DeleteMapping("/produto/{id}")
    @ApiOperation(value="Deleta um produto buscado por id")
    public void deletaProduto(@PathVariable(value="id") long id) {
        if(produtoRepository.findById(id) != null) {
            produtoRepository.deleteById(id);
        }
    }

    @PutMapping("/produto")
    @ApiOperation(value="Atualiza um produto")
    public Produto atualizaProduto(@RequestBody @Validated Produto p) {
        if(produtoRepository.findById(p.getId()) != null) {
            return produtoRepository.save(p);
        } else {
            return null;
        }
    }
}
