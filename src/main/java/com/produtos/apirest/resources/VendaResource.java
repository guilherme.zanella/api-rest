package com.produtos.apirest.resources;

import com.produtos.apirest.models.Venda;
import com.produtos.apirest.repository.VendaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api")
@CrossOrigin(origins = "*")
public class VendaResource {
    @Autowired
    VendaRepository vendaRepository;

    @GetMapping("/vendas")
    public List<Venda> listaVendas() {
        return vendaRepository.findAll();
    }

    @GetMapping("/venda/{id}")
    public Venda buscaVenda(@PathVariable(value = "id") long id){
        return vendaRepository.findById(id);
    }

    @PostMapping("/venda")
    public Venda salvaVenda(@RequestBody @Validated Venda venda) {
        return vendaRepository.save(venda);
    }

    @DeleteMapping("/venda/{id}")
    public void deletaVenda(@PathVariable(value = "id") long id) {
        if (vendaRepository.findById(id) != null) {
            vendaRepository.deleteById(id);
        }
    }

    @PutMapping("/venda")
    public Venda alteraVenda(@RequestBody @Validated Venda venda) {
        if (vendaRepository.findById(venda.getId()) != null) {
            return vendaRepository.save(venda);
        } else {
            return null;
        }
    }
}
