package com.produtos.apirest.resources;

import com.produtos.apirest.models.UnidadeDeMedida;
import com.produtos.apirest.repository.UnidadeMedidaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api")
@CrossOrigin(origins = "*")
public class UnidadeMedidaResource {
    @Autowired
    UnidadeMedidaRepository unidadeMedidaRepository;

    @GetMapping("/unidades-medida")
    public List<UnidadeDeMedida> listaUnidadesDeMedida() {
        return unidadeMedidaRepository.findAll();
    }

    @GetMapping("/unidade-medida/{id}")
    public UnidadeDeMedida unidadeDeMedidaPorId(@PathVariable(value="id") long id) {
        return unidadeMedidaRepository.findById(id);
    }

    @PostMapping("/unidade-medida")
    public UnidadeDeMedida salvaUnidadeDeMedida(@RequestBody @Validated UnidadeDeMedida um) {
        return unidadeMedidaRepository.save(um);
    }

    @DeleteMapping("/unidade-medida/{id}")
    public void deletaUnidadeDeMedida(@PathVariable(value="id") long id) {
        if(unidadeMedidaRepository.findById(id) != null) {
            unidadeMedidaRepository.deleteById(id);
        }
    }

    @PutMapping("/unidade-medida")
    public UnidadeDeMedida atualizaUnidadeMedida(@RequestBody @Validated UnidadeDeMedida um) {
        if(unidadeMedidaRepository.findById(um.getId()) != null) {
            return unidadeMedidaRepository.save(um);
        } else {
            return null;
        }
    }
}
