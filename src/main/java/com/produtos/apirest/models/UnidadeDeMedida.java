package com.produtos.apirest.models;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name="unidades_medida")
public class UnidadeDeMedida {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private long id;
    @NotNull
    private String sigla;
    @NotNull
    private String descricao;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getSigla() {
        return sigla;
    }
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
