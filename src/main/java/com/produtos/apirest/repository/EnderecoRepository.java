package com.produtos.apirest.repository;

import com.produtos.apirest.models.Endereco;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnderecoRepository extends JpaRepository<Endereco, Long> {
    Endereco findById(long id);
}
