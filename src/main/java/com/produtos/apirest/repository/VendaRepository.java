package com.produtos.apirest.repository;

import com.produtos.apirest.models.Venda;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendaRepository extends JpaRepository<Venda, Long> {
    Venda findById(long id);
}
