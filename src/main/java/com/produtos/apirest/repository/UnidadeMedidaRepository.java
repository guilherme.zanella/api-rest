package com.produtos.apirest.repository;

import com.produtos.apirest.models.UnidadeDeMedida;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnidadeMedidaRepository extends JpaRepository<UnidadeDeMedida, Long> {
    UnidadeDeMedida findById(long id);
}
